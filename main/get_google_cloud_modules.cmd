#!/bin/bash

npm install @google-cloud/language@^0.10.6
npm install @google-cloud/speech@^0.10.2
npm install @google-cloud/translate@^0.7.0
npm install @google-cloud/vision@^0.10.0
